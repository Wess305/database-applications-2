package com.example.Application;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
public class AccountController {

	@Autowired
	private AccountRepository accountRepository;
	
	@GetMapping
	public Iterable<Account> findAll() {
		return accountRepository.findAll();
	}
	
	@GetMapping("/accountNumber/")
	public List<Account> findByaccountNumber(@PathVariable int accountNumber) {
		return accountRepository.findByaccountNumber(accountNumber);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Account create(@RequestBody Account account) {
		return accountRepository.save(account);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		accountRepository.findById(id).orElseThrow(IllegalArgumentException::new);
		accountRepository.deleteById(id);
	}
}
