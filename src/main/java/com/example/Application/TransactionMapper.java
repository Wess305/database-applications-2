package com.example.Application;

import org.mapstruct.Mapper;

@Mapper
public interface TransactionMapper {
	TransactionDTO toDTO(Transaction transaction);

}

