package com.example.Application;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AddressMapper {

	@Mapping(source = "address.streetNum", target = "streetNumber") 
	AddressDTO toDTO(Address address);
	
}

