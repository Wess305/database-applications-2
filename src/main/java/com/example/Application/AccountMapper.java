package com.example.Application;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AccountMapper {

	@Mapping(source="account.transactions", target="transactionsDTO")
	AccountDTO toDTO(Account account);
	

}