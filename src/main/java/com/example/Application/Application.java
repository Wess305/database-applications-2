package com.example.Application;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public CommandLineRunner demo(CustomerRepository customerRepository, AccountRepository accountRepository,
			TransactionRepository transactionRepository, AddressRepository addressRepository) {
		return (args) -> {
			
		
			Customer customer1 = new Customer("Wesley", "Guni");
			customerRepository.save(customer1);
			
			Address address1 = new Address(000001, 25, "Ruska", "Wroclaw", "Poland");
			addressRepository.save(address1);
			
			Account account1 = new Account(123456789, "Wesley", 7000);
			accountRepository.save(account1);
			
			Transaction transaction1 = new Transaction(185, "18th August");
			transactionRepository.save(transaction1);
			
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : customerRepository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");


			log.info("Accounts found with findAll():");
			log.info("-------------------------------");
			for (Account account : accountRepository.findAll()) {
				log.info(account.toString());
			}
			log.info("");


			

		};
			
	}

}


