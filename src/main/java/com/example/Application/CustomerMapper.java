package com.example.Application;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper
public interface CustomerMapper {
	 @Mapping(source = "customer.firstName", target = "name")
	 CustomerDTO toDTO(Customer customer);
	
}
