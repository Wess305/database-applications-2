package com.example.Application;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customers")
public class CustomerController {
	
	@Autowired
	private CustomerRepository customerRepository;

	@GetMapping()
	public Iterable<Customer> findAll() {
		return customerRepository.findAll();
	}
	@GetMapping("/lastname/{name}")
	public List<Customer> findByLastName(@PathVariable String name) {
		return customerRepository.findByLastName(name);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Customer create(@RequestBody Customer customer) {
		return customerRepository.save(customer);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		customerRepository.findById(id).orElseThrow(IllegalArgumentException::new);
		customerRepository.deleteById(id);
	}




}
